/**
 * Created by ankushthapa on 02/08/16.
 */

var jgPayments = angular.module('jgPayments', ["ngRoute"]);

jgPayments.config(function($routeProvider){
  $routeProvider
    // package listing page
    .when('/packages-listing/:packageId/',{
      controller: 'packagesCtrl',
      templateUrl: 'templates/packageListingPage.html',
    })

    // payment page url
    .when('/payments/:packageId/',{
      controller: 'paymentsCtrl',
      templateUrl: 'templates/paymentsPage.html'
    })

    // payment success url
    .when('/payment-success/',{
      templateUrl: 'templates/paymentSuccess.html'
    })

    // default value
    .otherwise({
      redirectTo: '/packages-listing/-1/'
    });
});
