/**
 * Created by ankushthapa on 02/08/16.
 */

jgPayments.controller('packagesCtrl', function($scope){

  // fetch packages listed in constants, although in real scenario these should be fetched from backend
  $scope.allPackages = allPackages;

  // sample promo coupons
  $scope.promoCodes = promoCodes;

  // maintaining the previous state when a package is selected
  $scope.selectPackage = function(packageId){
    window.location.href = window.location.href.split('/packages-listing/')[0] + '/packages-listing/' + packageId + '/';
  };

  var setSelectedPackage = function(){
    var href = window.location.href.split('/')
    $scope.selectedPackage = href[href.length-2];
  }
  setSelectedPackage();


  // this will create a dummy package for handeling ad-hoc amount from user
  $scope.addPackage = function(packageId){
    var newAmountValue = document.getElementById('newAmountValue').value;

    if(packageId == -1){
      if(!newAmountValue){
        alert('enter amount please');
        return;
      }
      allPackages.push({id: -1, amount: newAmountValue, active: true})
    }
    window.location.href = window.location.href.split('/packages-listing/')[0] + '/payments/' + packageId + '/';
  }
});
