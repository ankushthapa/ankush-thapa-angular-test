/**
 * Created by ankushthapa on 02/08/16.
 */

jgPayments.controller('paymentsCtrl', function($scope){
  $scope.allPackages = allPackages;
  $scope.something = 'somethinasdad';

  // this will set value of selected package in scope to be accessible by the view
  var setSelectedPackage = function(){
    var href = window.location.href.split('/')
    $scope.selectedPackage = href[href.length-2];
  }
  setSelectedPackage();

  // hover action over payment methods
  $scope.hoverAction = function(elementId){
    var element = document.getElementById(elementId);
    if(element.style.backgroundColor == 'grey'){
      element.style.backgroundColor = 'lightgrey';
      element.style.color = 'grey';
    }
    else if (element.style.backgroundColor == 'lightgrey'){
      element.style.backgroundColor = 'grey';
      element.style.color = 'lightgrey';
    }
  }

  // fetching payment methods from constants, ideally should be fetched from backend server
  $scope.paymentMethods = paymentMethods;

  // handeling payment
  $scope.handlePayment = function(elementId){
    // there should be better ways to identify selected payment method
    for(i=0;i<paymentMethods.length;i++){
      if(paymentMethods[i].id != elementId){
        document.getElementById(paymentMethods[i].id).style.backgroundColor = 'lightgrey';
        document.getElementById(paymentMethods[i].id).style.color = 'grey';
      }
      else{
        document.getElementById(paymentMethods[i].id).style.backgroundColor = 'maroon';
      }
    }
    if(elementId == 'PM1'){
      $scope.paymentOptions = ['Master Card', 'Visa Card', 'American Express', 'PayTM Bank'];
    }
    else if(elementId == 'PM2'){
      $scope.paymentOptions = ['Sbi Card', 'Citi Bank', 'HDFC Bank', 'SBH Bank'];
    }
    else if(elementId == 'PM3'){
      $scope.paymentOptions = ['State Bank of India', 'State Bank of Delhi', 'State Bank of Nowhere'];
    }
    else if(elementId == 'PM4'){
      $scope.paymentOptions = ['Wallet 1', 'Wallet 2', 'Wallet 3', 'Wallet 4'];
    }
    else if(elementId == 'PM5'){
      $scope.paymentOptions = ['Cash Card 1', 'Cash Card 2', 'Cash Card 3', 'Cash Card 4'];
    }
  }

  // set true if a payment method is selected else false
  $scope.selectAnOption = function(){
    $scope.selectedOption = true;
  };

});
