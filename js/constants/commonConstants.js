
allPackages = [
  {id:1, amount: 100, bonus: 20, active: true},
  {id:2, amount: 200, bonus: 40, active: true},
  {id:3, amount: 300, bonus: 0, active: true},
  {id:4, amount: 400, bonus: 80, active: true},
  {id:5, amount: 500, bonus: 100, active: true},
  {id:6, amount: 600, bonus: 120, active: false},
];

paymentMethods = [
  {id:'PM1', name:'Credit Card'},
  {id:'PM2', name:'Debit Card'},
  {id:'PM3', name:'Internet Banking'},
  {id:'PM4', name:'Wallets'},
  {id:'PM5', name:'Cash Cards'},
];

 promoCodes = ['Promo Coupon 1', 'Promo Coupon 2', 'Promo Coupon 3', 'Promo Coupon 4', 'Promo Coupon 5'];
