This is a simple angular program which will assist in payment in a mobile gaming platform.

Since browser may not load html pages over GET requests locally, you may need to start any sort of http server.
Simply go into the project folder and start a python simple http server - python -m SimpleHTTPServer 8000. Now you have an http server running and simply open http://localhost:8000/ and it will load the index.html file of the project by default.

First page of this application basically shows different set of existin packages, and once customisable amount. These packages are defined locally in constants while ideally these should be fetched from a backend server in real scenario.

Once a package is chosen, state of this action is maintained. This means if you choose another package after choosing one, simple back action of browser will reselect the previously selected package and forward button will do the reverse. 

Once you proceed to payments page, you are promted to choose payment option. After payment option is chosen, you can proceed with the payment. 